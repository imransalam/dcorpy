# DCorpy

## Pre Requisites
- In the params.py, chose either any of these two frameworks; Pytorch / Keras
```python
framework = 'keras' ### or 'pytorch'
```
- Download weights file for VGG19 when using Keras with this link https://drive.google.com/file/d/1CTkwDxmT438FHP9d_uRAeerN-G-M4pbf/view?usp=sharing
- Download weights file for VGG19 when using pytorch
```bash
wget https://download.pytorch.org/models/vgg19-dcbb9e9d.pth
```

## Usage
```python
python synth.py
```

## Status
Currently stuck in do_back_prop when I have to call convnet function to get dzdx
