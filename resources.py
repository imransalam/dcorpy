
import numpy as np
import copy
from scipy import signal


def forward_prop(net, target_image, params):
    
    if params.framework == 'keras':
        target_image = target_image[np.newaxis,...]
        truncated_net = copy.copy(net)
        truncated_net.layers = net.layers[:params.lastLayerNum+1]

        inp = truncated_net.input
        outputs = [layer.output for layer in truncated_net.layers]
        functors = params.backend.function([inp]+ [params.backend.learning_phase()], outputs )
        forward_res = functors([target_image, 1.])

    else:

        target_image = np.transpose(target_image, (2,0,1))
        target_image = target_image[np.newaxis,...]
        fn = net.forward(params.backend.tensor(target_image).float())
        forward_res = copy.copy(net.output[:params.lastLayerNum+1])

        for i in range(len(forward_res)):
            forward_res[i] = np.transpose(forward_res[i].detach().numpy(), (0, 2, 3, 1))

    return forward_res


def calc_style_error_loss(target_style_features, src_gram_mats, params):

    num_feature_layers = len(params.styleMatchLayerInds)
    style_error_loss = 0.0
    style_grads = []
    for k in range(num_feature_layers):
        feature_matrix_shape = target_style_features[k].shape
        if len(feature_matrix_shape) == 2:
            feature_matrix_shape = feature_matrix_shape + (1,)

        target_gram_matrix, target_feature_vector = calc_gram_matrix(target_style_features[k].astype(np.float64))
        src_gram_matrix = src_gram_mats[k]
        M = float(feature_matrix_shape[0] * feature_matrix_shape[1])
        N = float(feature_matrix_shape[2])

        src_gram_matrix = src_gram_matrix/M
        target_gram_matrix = target_gram_matrix/M

        style_diff = target_gram_matrix - src_gram_matrix
        style_diff_weight = params.styleFeatureWeights[k]

        style_error_loss = style_error_loss + (style_diff_weight/4.0) * (np.sum( np.power(style_diff,2).astype(float) )) / np.power(N,2).astype(float)

        aux = np.transpose(np.dot(np.transpose(target_feature_vector) , style_diff)) / (M*np.power(N,2))

        style_grads.append(np.dot(style_diff_weight , np.reshape( aux.T.astype(np.float32), feature_matrix_shape) ) )

    return style_error_loss, style_grads

def calc_a_corr_grad(feature_mat, a_corr_diff):
    num_features = feature_mat.shape[2]
    feature_size = (feature_mat.shape[0], feature_mat.shape[1])
    a_corr_grad = np.zeros(feature_mat.shape)
    weights_out_x, weights_out_y = np.meshgrid(
        np.append(np.arange(int(np.round((feature_size[0]/2))),feature_size[0], 1) , np.arange(feature_size[0], int(np.round((feature_size[0]/2))), -1)),
        np.append(np.arange(int(np.round((feature_size[0]/2))),feature_size[0], 1) , np.arange(feature_size[0], int(np.round((feature_size[0]/2))), -1))
        )
    for ind in range(num_features):
        a_corr_grad[:,:,ind] = np.dot(4.0 , signal.convolve2d(np.double(np.flipud(np.fliplr(a_corr_diff[:,:,ind]))) / (weights_out_x * weights_out_y), np.double(feature_mat[:,:,ind]), mode='same'))
    return a_corr_grad


def calc_a_corr_error_loss(target_a_corr_features, src_a_corr_mats, params):
    num_feature_layers = len(params.ACorrMatchLayerInds)
    a_corr_grads = []
    a_corr_error_loss = 0.0
    for k in range(num_feature_layers):
        target_a_corr_tensor = calc_a_corr_tensor(target_a_corr_features[k])
        src_a_corr_tensor = src_a_corr_mats[k]
        a_corr_diff = target_a_corr_tensor - src_a_corr_tensor
        a_corr_diff_weight = params.ACorrFeatureWeights[k]
        a_corr_error_loss = a_corr_error_loss + a_corr_diff_weight * np.sum( np.power(a_corr_diff, 2) ) 
        a_corr_grads.append( a_corr_diff_weight * calc_a_corr_grad(target_a_corr_features[k], a_corr_diff) )
    return a_corr_error_loss, a_corr_grads

def calc_structure_error_loss(target_diversity_features, src_diversity_features, params):
    num_features = len(params.DiversityMatchLayerInds)
    diversity_grads = []
    diversity_error_loss = 0.0
    for k in range(num_features):
        feature_diff = target_diversity_features[k] - src_diversity_features[k]
        diversity_diff_weight = params.DiversityFeatureWeights[k]
        diversity_error_loss = 0.5 * np.sum(np.power(feature_diff,2))
        diversity_grads.append( np.dot(diversity_diff_weight , feature_diff) )
    return diversity_error_loss, diversity_grads

def calc_soft_min_smoothness_error_loss(target_smoothness_features, params):
    num_features = len(params.SmoothnessMatchLayerInds)
    smoothness_grads = []
    smoothness_error_loss = 0.0
    sig = params.SmoothnessSigma
    for k in range(num_features):
        tsf = target_smoothness_features[k].astype(np.float64)
        smoothness_diff_up = tsf - np.roll(tsf, -1, axis=0)
        smoothness_diff_down = tsf - np.roll(tsf, 1, axis=0)
        smoothness_diff_left = tsf - np.roll(tsf, -1, axis=1)
        smoothness_diff_right = tsf - np.roll(tsf, 1, axis=1)

        smoothness_diff_weight = params.SmoothnessFeatureWeights[k]
        try:
            smoothness_error_loss = smoothness_error_loss + smoothness_diff_weight * (1.0/sig) * 0.5 * (
                np.sum(
                    np.log(
                        (0.25 * np.exp( (-sig) * np.power(smoothness_diff_up, 2.0) )) +
                        (0.25 * np.exp( (-sig) * np.power(smoothness_diff_down, 2.0) )) +
                        (0.25 * np.exp( (-sig) * np.power(smoothness_diff_left, 2.0) )) +
                        (0.25 * np.exp( (-sig) * np.power(smoothness_diff_right, 2.0) ))
                        )
                    )
                )
            nom1 = (
                smoothness_diff_up * np.exp( (-sig) * np.power(smoothness_diff_up, 2.0) ) +
                smoothness_diff_down * np.exp( (-sig) * np.power(smoothness_diff_down, 2.0) ) +
                smoothness_diff_left * np.exp( (-sig) * np.power(smoothness_diff_left, 2.0) ) +
                smoothness_diff_right * np.exp( (-sig) * np.power(smoothness_diff_right, 2.0) ))
            den1 = (
                np.exp( (-sig) * np.power(smoothness_diff_up, 2.0) ) +
                np.exp( (-sig) * np.power(smoothness_diff_down, 2.0) ) +
                np.exp( (-sig) * np.power(smoothness_diff_left, 2.0) ) +
                np.exp( (-sig) * np.power(smoothness_diff_right, 2.0) ))
            
            dummy_nom_up = ( np.roll(tsf, -1, axis=0) - tsf )
            dummy_den_up_up = ( np.roll(tsf, -1, axis=0) - np.roll(tsf, -1-1, axis=0) )
            dummy_den_up_down = ( np.roll(tsf, -1, axis=0) - np.roll(tsf, -1+1, axis=0) )
            dummy_den_up_left = ( np.roll(tsf, -1, axis=0) - np.roll(np.roll(tsf, -1 ,axis=1), -1, axis=0) )
            dummy_den_up_right = ( np.roll(tsf, -1, axis=0) - np.roll(np.roll(tsf, 1 ,axis=1), -1, axis=0) )
            nom_up = dummy_nom_up * np.exp( (-sig) * np.power(dummy_nom_up, 2.0) )
            den_up = np.exp( (-sig) * np.power(dummy_den_up_up, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_up_down, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_up_left, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_up_right, 2.0) )

            dummy_nom_down = ( np.roll(tsf, 1, axis=0) - tsf )
            dummy_den_down_up = ( np.roll(tsf, 1, axis=0) - np.roll(tsf, +1-1, axis=0) )
            dummy_den_down_down = ( np.roll(tsf, 1, axis=0) - np.roll(tsf, +1+1, axis=0) )
            dummy_den_down_left = ( np.roll(tsf, 1, axis=0) - np.roll(np.roll(tsf, -1 ,axis=1), 1, axis=0) )
            dummy_den_down_right = ( np.roll(tsf, 1, axis=0) - np.roll(np.roll(tsf, 1 ,axis=1), 1, axis=0) )
            nom_down = dummy_nom_down * np.exp( (-sig) * np.power(dummy_nom_down, 2.0) )
            den_down = np.exp( (-sig) * np.power(dummy_den_down_up, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_down_down, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_down_left, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_down_right, 2.0) )

            dummy_nom_left = ( np.roll(tsf, -1, axis=1) - tsf )
            dummy_den_left_up = ( np.roll(tsf, -1, axis=1) - np.roll(np.roll(tsf, -1 ,axis=1), -1, axis=0) )
            dummy_den_left_down = ( np.roll(tsf, -1, axis=1) - np.roll(np.roll(tsf, -1 ,axis=1), 1, axis=0) )
            dummy_den_left_left = ( np.roll(tsf, -1, axis=1) - np.roll(tsf, -2, axis=1) )
            dummy_den_left_right = ( np.roll(tsf, -1, axis=1) - np.roll(tsf, 0, axis=0) )
            nom_left = dummy_nom_left * np.exp( (-sig) * np.power(dummy_nom_left, 2.0) )
            den_left = np.exp( (-sig) * np.power(dummy_den_left_up, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_left_down, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_left_left, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_left_right, 2.0) )

            dummy_nom_right = ( np.roll(tsf, 1, axis=1) - tsf )
            dummy_den_right_up = ( np.roll(tsf, 1, axis=1) - np.roll(np.roll(tsf, 1 ,axis=1), -1, axis=0) )
            dummy_den_right_down = ( np.roll(tsf, 1, axis=1) - np.roll(np.roll(tsf, 1 ,axis=1), 1, axis=0) )
            dummy_den_right_left = ( np.roll(tsf, 1, axis=1) - np.roll(tsf, 0, axis=0) )
            dummy_den_right_right = ( np.roll(tsf, 1, axis=1) - np.roll(tsf, 2, axis=1) )
            nom_right = dummy_nom_right * np.exp( (-sig) * np.power(dummy_nom_right, 2.0) )
            den_right = np.exp( (-sig) * np.power(dummy_den_right_up, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_right_down, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_right_left, 2.0) ) + np.exp( (-sig) * np.power(dummy_den_right_right, 2.0) )
            
            smoothness_grads.append(smoothness_diff_weight * (- (nom1 / den1) + (nom_up / den_up) + (nom_down / den_down) + (nom_left / den_left) + (nom_right / den_right) ))
        except Exception as error:
            print (error)
            smoothness_error_loss = 0.0
            smoothness_grads.append(np.zeros(tsf.shape))
        return smoothness_error_loss, smoothness_grads

def combine_grads(style_grads, a_corr_grads, diversity_grads, smoothness_grads, params):
    grads = []
    for k in range(len(params.unitedLayerInds)):
        grads.append(0)
        
        aux_ind = np.where(params.unitedLayerInds[k] == np.array(params.styleMatchLayerInds))[0]
        if len(aux_ind) > 0:
            grads[-1] = grads[-1] + params.styleLossWeight * style_grads[aux_ind[0]]

        aux_ind = np.where(params.unitedLayerInds[k] == np.array(params.ACorrMatchLayerInds))[0]
        if len(aux_ind) > 0:
            grads[-1] = grads[-1] + params.ACorrLossWeight * a_corr_grads[aux_ind[0]]

        aux_ind = np.where(params.unitedLayerInds[k] == np.array(params.DiversityMatchLayerInds))[0]
        if len(aux_ind) > 0 and params.DiversityLossWeight != 0:
            grads[-1] = grads[-1] + params.DiversityLossWeight * diversity_grads[aux_ind[0]]

        aux_ind = np.where(params.unitedLayerInds[k] == np.array(params.SmoothnessMatchLayerInds))[0]
        if len(aux_ind) > 0:
            grads[-1] = grads[-1] + params.SmoothnessLossWeight * smoothness_grads[aux_ind[0]]
    return grads



def calc_error_loss(src_mats, target_features, params, verbose):
    
    style_error_loss, style_grads = calc_style_error_loss(target_features['style_features'], src_mats['gram_mats'], params)    
    if verbose:
        print ('Alpha * Style Error Loss = ', str(params.styleLossWeight*style_error_loss))

    if params.ACorrLossWeight != 0:
        a_corr_error_loss, a_corr_grads = calc_a_corr_error_loss(target_features['a_corr_features'], src_mats['a_corr_mats'], params)
        if verbose:
            print ('Beta * ACorr Error Loss = ', str(params.ACorrLossWeight*a_corr_error_loss))
    else:
        a_corr_error_loss, a_corr_grads = 0.0, []

    if params.DiversityLossWeight != 0:
        diversity_error_loss, diversity_grads = calc_structure_error_loss(target_features['diversity_features'], src_mats['diversity_mats'], params)
        if verbose:
            print ('Delta * Diversity Error Loss = ', str(params.DiversityLossWeight*diversity_error_loss))
    else:
        diversity_error_loss, diversity_grads = 0.0, []

    if params.SmoothnessLossWeight != 0 :
        smoothness_error_loss, smoothness_grads = calc_soft_min_smoothness_error_loss(target_features['smoothness_features'], params)
        if verbose:
            print ('Gamma * Smoothness Error Loss = ', str(params.SmoothnessLossWeight*smoothness_error_loss))
    else:
        smoothness_error_loss, smoothness_grads = 0.0, []

    error_loss = params.styleLossWeight * style_error_loss + params.ACorrLossWeight * a_corr_error_loss + params.DiversityLossWeight * diversity_error_loss + params.SmoothnessLossWeight * smoothness_error_loss
    grads = combine_grads(style_grads, a_corr_grads, diversity_grads, smoothness_grads, params)
    return error_loss, grads

def calc_a_corr_tensor_full(src_feature_matrix, target_feature_matrix):
    num_features = src_feature_matrix.shape[2]
    feature_size = (src_feature_matrix.shape[0], src_feature_matrix.shape[1])
    a_corr_tensor = np.zeros((target_feature_matrix.shape[0],target_feature_matrix.shape[1],target_feature_matrix.shape[2]))
    weights_in_x, weights_in_y = np.meshgrid(
        np.append(np.arange(0,feature_size[0]-1,1) , np.arange(feature_size[0]-1,-1,-1)), 
        np.append(np.arange(0,feature_size[0]-1,1) , np.arange(feature_size[0]-1,-1,-1))
        )
    for ind in range(num_features):
        dummy_a_corr = signal.correlate2d(src_feature_matrix[:,:,ind], src_feature_matrix[:,:,ind])
        dummy_a_corr = dummy_a_corr / (weights_in_x * weights_in_y)
        mid_coord = np.ceil( np.array(dummy_a_corr.shape) / 2 ).astype(int)
        enlargement = np.floor((target_feature_matrix.shape[0] - src_feature_matrix.shape[0]) / 2).astype(int)
        dummy_a_corr = dummy_a_corr[ mid_coord[0] - np.floor(feature_size[0]/2).astype(int) - enlargement : mid_coord[0] - np.floor(feature_size[0]/2).astype(int) + feature_size[0] + enlargement, mid_coord[1] - np.floor(feature_size[1]/2).astype(int) - enlargement : mid_coord[1] - np.floor(feature_size[1]/2).astype(int) + feature_size[1] + enlargement]
        a_corr_tensor[:,:,ind] = dummy_a_corr
    return a_corr_tensor

def calc_a_corr_tensor(feature_matrix):
    num_features = feature_matrix.shape[2]
    feature_size = (feature_matrix.shape[0], feature_matrix.shape[1])
    a_corr_tensor = np.zeros((feature_matrix.shape[0],feature_matrix.shape[1],feature_matrix.shape[2]))

    weights_out_x, weights_out_y = np.meshgrid(
        np.append(np.arange(int(np.round((feature_size[0]/2))),feature_size[0], 1) , np.arange(feature_size[0], int(np.round((feature_size[0]/2))), -1)),
        np.append(np.arange(int(np.round((feature_size[0]/2))),feature_size[0], 1) , np.arange(feature_size[0], int(np.round((feature_size[0]/2))), -1))
        )
    for ind in range(num_features):
        dummy_a_corr = signal.correlate2d(feature_matrix[:,:,ind], feature_matrix[:,:,ind])
        mid_coord = np.ceil( np.array(dummy_a_corr.shape) / 2 ).astype(int)
        dummy_a_corr = dummy_a_corr[ mid_coord[0] - np.floor(feature_size[0]/2).astype(int) : mid_coord[0] - np.floor(feature_size[0]/2).astype(int) + feature_size[0], mid_coord[1] - np.floor(feature_size[1]/2).astype(int) : mid_coord[1] - np.floor(feature_size[1]/2).astype(int) + feature_size[1]]
        dummy_a_corr = dummy_a_corr / (weights_out_x * weights_out_y)
        a_corr_tensor[:,:,ind] = dummy_a_corr
    return a_corr_tensor

def calc_gram_matrix(feature_matrix):
    shape_feature_matrix = feature_matrix.shape
    if len(shape_feature_matrix) == 2:
        shape_feature_matrix = shape_feature_matrix + (1,)
    feature_vector = np.reshape( np.transpose(feature_matrix, (2,0,1)), (shape_feature_matrix[2],  shape_feature_matrix[0] * shape_feature_matrix[1]))
    gram_matrix = np.dot(feature_vector , np.transpose(feature_vector))
    return gram_matrix, feature_vector


def calc_net_features(net, img, params):
    # truncated_net = copy.copy(net)
    # truncated_net.layers = net.layers[:params.lastLayerNum+1]

    # inp = truncated_net.input
    # outputs = [layer.output for layer in truncated_net.layers]
    # functors = K.function([inp]+ [K.learning_phase()], outputs )

    # img = img[np.newaxis,...]
    # new_res = functors([img, 1.])

    new_res = forward_prop(net, img, params)

    style_features = []
    for k in range(len(params.styleMatchLayerInds)):
        style_features.append( new_res[params.styleMatchLayerInds[k] + 1][0] )

    a_corr_features = []
    for k in range(len(params.ACorrMatchLayerInds)):
        a_corr_features.append( new_res[params.ACorrMatchLayerInds[k] + 1][0] )


    diversity_features = []
    for k in range(len(params.DiversityMatchLayerInds)):
        diversity_features.append( new_res[params.DiversityMatchLayerInds[k] + 1][0] )


    smoothness_features = []
    for k in range(len(params.SmoothnessMatchLayerInds)):
        smoothness_features.append( new_res[params.SmoothnessMatchLayerInds[k] + 1][0] )

    return style_features, a_corr_features, diversity_features, smoothness_features


def get_src_matrices(src_img, init_noise, net, params):

    style_features, a_corr_features, diversity_features, smoothness_features = calc_net_features(net, src_img, params)
    src_features = {
        'style_features' : style_features,
        'a_corr_features' : a_corr_features,
        'diversity_features' : diversity_features,
        'smoothness_features' : smoothness_features
    }

    _, a_corr_features, _, _ = calc_net_features(net, init_noise, params)
    target_features = {
        'a_corr_features' : a_corr_features
    }

    gram_mats = []
    for ind in range(len(params.styleMatchLayerInds)):
        gm,_ = calc_gram_matrix(src_features['style_features'][ind])
        gram_mats.append( gm )

    a_corr_mats = []
    for ind in range(len(params.ACorrMatchLayerInds)):
        a_corr_mats.append( calc_a_corr_tensor_full( src_features['a_corr_features'][ind], target_features['a_corr_features'][ind] ) )

    diversity_mats = []
    for ind in range(len(params.DiversityMatchLayerInds)):
        diversity_mats.append( src_features['diversity_features'][ind] )

    src_mats = {
    'gram_mats' : gram_mats,
    'a_corr_mats' : a_corr_mats,
    'diversity_mats' : diversity_mats
    }

    return src_mats

def get_loss_from_image(net, cur_target_image, src_img, src_mats, params):
    global save_data_prev_errrors
    global save_data_prev_deltas
    global saved_data_prev_img

    if (params.autoconverge==1 and np.prod( np.array(save_data_prev_deltas).shape ) >= params.autoconvergelen+1 ):
        end = save_data_prev_errrors[-1]
        if ( np.prod( save_data_prev_deltas[end - params.autoconvergelen : end] <= params.autoconvergethresh ) ):
            error_loss = 0
            print ('Auto Converge')
            return error_loss

    cur_target_image = np.reshape( cur_target_image, ( np.round(params.USFac * src_img.shape[0]).astype(int), np.round(params.USFac * src_img.shape[1]), src_img.shape[2] ) )
    style_features, a_corr_features, diversity_features, smoothness_features = calc_net_features(net, cur_target_image, params)

    target_features = {
        'style_features' : style_features,
        'a_corr_features' : a_corr_features,
        'diversity_features' : diversity_features,
        'smoothness_features' : smoothness_features
    }

    error_loss, _ = calc_error_loss(src_mats, target_features, params, 1)

    if len(saved_data_prev_img) > 0:
        curr_max_pix_diff = np.max(np.abs(saved_data_prev_img - cur_target_image))
    else:
        curr_max_pix_diff = 0

    save_data_prev_errrors = np.append(save_data_prev_errrors , error_loss)
    save_data_prev_deltas = np.append(save_data_prev_deltas, error_loss)
    saved_data_prev_img = cur_target_image
    ####################################
    # SAVE PREV_IMAGE AND SOURCE.
    ####################################
    return error_loss

def do_back_prop(net, grads, target_image, params):
    # truncated_net = copy.copy(net)
    # truncated_net.layers = net.layers[:params.lastLayerNum+1]

    # inp = truncated_net.input
    # outputs = [layer.output for layer in truncated_net.layers]
    # functors = K.function([inp]+ [K.learning_phase()], outputs )

    # target_image = target_image[np.newaxis,...]
    # forward_res = functors([target_image, 1.])
    forward_res = forward_prop(net, target_image, params)

    print ("Backward Pass")
    dx = 0.0
    for k in range(params.lastLayerNum - 1, -1, -1):
        aux_ind = np.where( np.array(params.unitedLayerInds) == k )[0]
        if len(aux_ind) < 1:
            continue
        aux_ind = aux_ind[0]

        truncated_net.layers = net.layers[:k]
        cur_res = forward_res[0: k + 1]
        cur_res_plus_one_grad = grads[aux_ind]
        


def get_grad(net, cur_target_image, src_img, src_mats, params):
    global saved_data_prev_deltas

    if (params.autoconverge==1 and np.prod( np.array(save_data_prev_deltas).shape ) >= params.autoconvergelen+1 ):
        end = save_data_prev_errrors[-1]
        if ( np.prod( save_data_prev_deltas[end - params.autoconvergelen : end] <= params.autoconvergethresh ) ):
            grad = np.zeros_like(cur_target_image)
            print ('Auto Converge')
            return error_loss
    
    cur_target_image = np.reshape( cur_target_image, ( np.round(params.USFac * src_img.shape[0]).astype(int), np.round(params.USFac * src_img.shape[1]), src_img.shape[2] ) )
    style_features, a_corr_features, diversity_features, smoothness_features = calc_net_features(net, cur_target_image, params)

    target_features = {
        'style_features' : style_features,
        'a_corr_features' : a_corr_features,
        'diversity_features' : diversity_features,
        'smoothness_features' : smoothness_features
    }
    error_loss, combined_grads = calc_error_loss(src_mats, target_features, params, 1)

    grad = do_back_prop(net, combined_grads, cur_target_image, params)

    