
import numpy as np

framework = 'keras'

if framework == 'keras':
    from keras import backend
    pretrained_vgg = 'vgg19-weights.h5'
else:
    import torch as backend
    pretrained_vgg = 'vgg19-weights.pth'



netImgSz = (224,224,3)
USFac = 1

styleMatchLayerInds = [4, 9, 18, 27]
styleFeatureWeights = np.array([1.0, 1.0, 1.0, 1.0])

ACorrMatchLayerInds = [9]
ACorrFeatureWeights = np.array([1.0])

SmoothnessMatchLayerInds = [0]
SmoothnessFeatureWeights = np.array([1.0])

DiversityMatchLayerInds = [9]
DiversityFeatureWeights = np.array([1.0])

styleFeatureWeights      = styleFeatureWeights/sum(styleFeatureWeights)
ACorrFeatureWeights      = ACorrFeatureWeights/sum(ACorrFeatureWeights)
DiversityFeatureWeights  = DiversityFeatureWeights/sum(DiversityFeatureWeights)

SmoothnessFeatureWeights = SmoothnessFeatureWeights/sum(SmoothnessFeatureWeights)
SmoothnessSigma          = 0.0001

lastLayerNum = max([np.max(styleMatchLayerInds), np.max(ACorrMatchLayerInds), np.max(SmoothnessMatchLayerInds), np.max(DiversityFeatureWeights)]) + 1
unitedLayerInds = np.unique(np.sort([styleMatchLayerInds + ACorrMatchLayerInds + SmoothnessMatchLayerInds + DiversityMatchLayerInds]))

NormGrads = 0
NormLosses = 0
Verbose = 1
NNVerbose = 1

styleLossWeight = 0.5
ACorrLossWeight =  0.5*1e-4
SmoothnessLossWeight = -0.0000075
DiversityLossWeight =  -1.0*1e-6

factr = 1e7
pgtol = 1e-5
printEvery = 5
maxIts = 300
m  = 5
autoconverge = 0
autoconvergelen = 50
autoconvergethresh = 0.5
