
import params
from resources import get_src_matrices, get_loss_from_image

if params.framework == 'keras':
    from vgg19_keras import VGG19
    net = VGG19(params.pretrained_vgg)
else:
    import torch
    from vgg19_pytorch import VGG19
    net = VGG19()
    net.load_state_dict(torch.load(params.pretrained_vgg))


import numpy as np
src_img = np.random.rand(224,224,3)
init_noise = np.random.rand(224,224,3) * 0.01

src_mats = get_src_matrices(src_img, init_noise, net, params)

e_loss = get_loss_from_image(net, init_noise, src_img, src_mats, params)


